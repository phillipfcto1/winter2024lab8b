import java.util.Random;
public class Board{
	private Tile[][] grid;
	final int boardSize = 5;
	Tile blank = Tile.BLANK;
	Tile hiddenWall = Tile.HIDDEN_WALL;
	Tile castle = Tile.CASTLE;
	Tile wall = Tile.WALL;
	public static void main(String[] args){
	}
	
	//make the array
	public Board(){
		//make a new grid array array and initialize every position to blank
		this.grid = new Tile[boardSize][boardSize];
		Random rand = new Random();
		
		for(int i = 0; i < this.grid.length; i++){
			int randWall = rand.nextInt(this.grid[i].length);
			
			for(int j = 0; j < this.grid[i].length;j++){
				if(j == randWall){
					this.grid[i][j] = hiddenWall;
				} else {
					this.grid[i][j] = blank;
				}
			}
			
		}
	}
	
	//make the board a single string + look like a 3x3
	public String toString(){
		
		//
		String gridLines = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length;j++){
				gridLines += this.grid[i][j].getName() + " "; 
		}
		gridLines += "\n";
		}
		return gridLines;
	}
	
	//place the token at the selected space
	public int placeToken(int row, int col){
		//checks if input by user is invalid -> returns false
		if( row < 0 || col < 0 || row >= boardSize || col >= boardSize){
			return -2;
		} else if (this.grid[row][col] == wall || this.grid[row][col] == castle){
			return -1;
		} else if(this.grid[row][col] == hiddenWall){
			this.grid[row][col] = wall;
			return 1;

		} else {
			this.grid[row][col] = castle;
			return 0;
		}
	}
	
	// public boolean checkIfWinning(Tile playerToken){
	// if(checkIfWinningHorizontal(playerToken)|| checkIfWinningfVertical(playerToken) || checkIfWinningDiagonalRight(playerToken) || checkIfWinningDiagonalLeft(playerToken)){
			// return true;
		// }
		// return false;
	// }
	
	
}