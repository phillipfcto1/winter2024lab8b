import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
	int numCastles = 7;
	int turns = 0;
	int row = -1;
	int col = -1;
	Board board = new Board();
	System.out.println("Hello friend, play a game with me why don't you");
	
		while(numCastles > 0 && turns < 8){
			System.out.println("Number of castles left: " + numCastles);
		
			System.out.println("Number of turns taken: " + turns);
		
			System.out.println(board);
		
			Scanner reader = new Scanner(System.in);
			System.out.println("Which row do you want to place your castle on?(0-4)");
			row = reader.nextInt();
			System.out.println("Which column do you want to place your castle on?(0-4)");
			col = reader.nextInt();
			int placeToken = board.placeToken(row, col);
		
			while(placeToken < 0){
				System.out.println("Please enter a valid number ");
				System.out.println("Which row do you want to place your castle on?(0-4)");
				row = reader.nextInt();
				System.out.println("Which column do you want to place your castle on?(0-4)");
				col = reader.nextInt();
				placeToken = board.placeToken(row, col);
			}
			
			if(placeToken == 1){
				System.out.println("There is a wall here you can't place anything");
			}else if(placeToken == 0){
				System.out.println("Castle successfuly placed my liege");
				numCastles --;
			}
			turns ++;
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("You have won congratulations");
		} else {
			System.out.println("Womp Womp you lost");
		}
	}


}